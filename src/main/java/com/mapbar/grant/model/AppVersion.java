package com.mapbar.grant.model;

import java.util.Date;

public class AppVersion {
    private String id;

    private String appId;

    private String packageName;

    private String md5;

    private String versionNo;

    private Float lowestOs;

    private Float highestOs;

    private Float size;

    private String iconPath;

    private String apkPath;

    private String imagePath;

    private Byte rank;

    private Integer status;

    private String versionDesc;

    private Float manulScore;

    private Date scoreDateStart;

    private Date scoreDateEnd;

    private Float scoreAvg;

    private Long downloadSum;

    private Integer newVersion;

    private Date createTime;

    private Date updateTime;

    private String createUser;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId == null ? null : appId.trim();
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName == null ? null : packageName.trim();
    }

    public String getMd5() {
        return md5;
    }

    public void setMd5(String md5) {
        this.md5 = md5 == null ? null : md5.trim();
    }

    public String getVersionNo() {
        return versionNo;
    }

    public void setVersionNo(String versionNo) {
        this.versionNo = versionNo == null ? null : versionNo.trim();
    }

    public Float getLowestOs() {
        return lowestOs;
    }

    public void setLowestOs(Float lowestOs) {
        this.lowestOs = lowestOs;
    }

    public Float getHighestOs() {
        return highestOs;
    }

    public void setHighestOs(Float highestOs) {
        this.highestOs = highestOs;
    }

    public Float getSize() {
        return size;
    }

    public void setSize(Float size) {
        this.size = size;
    }

    public String getIconPath() {
        return iconPath;
    }

    public void setIconPath(String iconPath) {
        this.iconPath = iconPath == null ? null : iconPath.trim();
    }

    public String getApkPath() {
        return apkPath;
    }

    public void setApkPath(String apkPath) {
        this.apkPath = apkPath == null ? null : apkPath.trim();
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath == null ? null : imagePath.trim();
    }

    public Byte getRank() {
        return rank;
    }

    public void setRank(Byte rank) {
        this.rank = rank;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getVersionDesc() {
        return versionDesc;
    }

    public void setVersionDesc(String versionDesc) {
        this.versionDesc = versionDesc == null ? null : versionDesc.trim();
    }

    public Float getManulScore() {
        return manulScore;
    }

    public void setManulScore(Float manulScore) {
        this.manulScore = manulScore;
    }

    public Date getScoreDateStart() {
        return scoreDateStart;
    }

    public void setScoreDateStart(Date scoreDateStart) {
        this.scoreDateStart = scoreDateStart;
    }

    public Date getScoreDateEnd() {
        return scoreDateEnd;
    }

    public void setScoreDateEnd(Date scoreDateEnd) {
        this.scoreDateEnd = scoreDateEnd;
    }

    public Float getScoreAvg() {
        return scoreAvg;
    }

    public void setScoreAvg(Float scoreAvg) {
        this.scoreAvg = scoreAvg;
    }

    public Long getDownloadSum() {
        return downloadSum;
    }

    public void setDownloadSum(Long downloadSum) {
        this.downloadSum = downloadSum;
    }

    public Integer getNewVersion() {
        return newVersion;
    }

    public void setNewVersion(Integer newVersion) {
        this.newVersion = newVersion;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser == null ? null : createUser.trim();
    }
}