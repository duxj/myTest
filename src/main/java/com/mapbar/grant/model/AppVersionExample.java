package com.mapbar.grant.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class AppVersionExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public AppVersionExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(String value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(String value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(String value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(String value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(String value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(String value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLike(String value) {
            addCriterion("id like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotLike(String value) {
            addCriterion("id not like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<String> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<String> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(String value1, String value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(String value1, String value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andAppIdIsNull() {
            addCriterion("app_id is null");
            return (Criteria) this;
        }

        public Criteria andAppIdIsNotNull() {
            addCriterion("app_id is not null");
            return (Criteria) this;
        }

        public Criteria andAppIdEqualTo(String value) {
            addCriterion("app_id =", value, "appId");
            return (Criteria) this;
        }

        public Criteria andAppIdNotEqualTo(String value) {
            addCriterion("app_id <>", value, "appId");
            return (Criteria) this;
        }

        public Criteria andAppIdGreaterThan(String value) {
            addCriterion("app_id >", value, "appId");
            return (Criteria) this;
        }

        public Criteria andAppIdGreaterThanOrEqualTo(String value) {
            addCriterion("app_id >=", value, "appId");
            return (Criteria) this;
        }

        public Criteria andAppIdLessThan(String value) {
            addCriterion("app_id <", value, "appId");
            return (Criteria) this;
        }

        public Criteria andAppIdLessThanOrEqualTo(String value) {
            addCriterion("app_id <=", value, "appId");
            return (Criteria) this;
        }

        public Criteria andAppIdLike(String value) {
            addCriterion("app_id like", value, "appId");
            return (Criteria) this;
        }

        public Criteria andAppIdNotLike(String value) {
            addCriterion("app_id not like", value, "appId");
            return (Criteria) this;
        }

        public Criteria andAppIdIn(List<String> values) {
            addCriterion("app_id in", values, "appId");
            return (Criteria) this;
        }

        public Criteria andAppIdNotIn(List<String> values) {
            addCriterion("app_id not in", values, "appId");
            return (Criteria) this;
        }

        public Criteria andAppIdBetween(String value1, String value2) {
            addCriterion("app_id between", value1, value2, "appId");
            return (Criteria) this;
        }

        public Criteria andAppIdNotBetween(String value1, String value2) {
            addCriterion("app_id not between", value1, value2, "appId");
            return (Criteria) this;
        }

        public Criteria andPackageNameIsNull() {
            addCriterion("package_name is null");
            return (Criteria) this;
        }

        public Criteria andPackageNameIsNotNull() {
            addCriterion("package_name is not null");
            return (Criteria) this;
        }

        public Criteria andPackageNameEqualTo(String value) {
            addCriterion("package_name =", value, "packageName");
            return (Criteria) this;
        }

        public Criteria andPackageNameNotEqualTo(String value) {
            addCriterion("package_name <>", value, "packageName");
            return (Criteria) this;
        }

        public Criteria andPackageNameGreaterThan(String value) {
            addCriterion("package_name >", value, "packageName");
            return (Criteria) this;
        }

        public Criteria andPackageNameGreaterThanOrEqualTo(String value) {
            addCriterion("package_name >=", value, "packageName");
            return (Criteria) this;
        }

        public Criteria andPackageNameLessThan(String value) {
            addCriterion("package_name <", value, "packageName");
            return (Criteria) this;
        }

        public Criteria andPackageNameLessThanOrEqualTo(String value) {
            addCriterion("package_name <=", value, "packageName");
            return (Criteria) this;
        }

        public Criteria andPackageNameLike(String value) {
            addCriterion("package_name like", value, "packageName");
            return (Criteria) this;
        }

        public Criteria andPackageNameNotLike(String value) {
            addCriterion("package_name not like", value, "packageName");
            return (Criteria) this;
        }

        public Criteria andPackageNameIn(List<String> values) {
            addCriterion("package_name in", values, "packageName");
            return (Criteria) this;
        }

        public Criteria andPackageNameNotIn(List<String> values) {
            addCriterion("package_name not in", values, "packageName");
            return (Criteria) this;
        }

        public Criteria andPackageNameBetween(String value1, String value2) {
            addCriterion("package_name between", value1, value2, "packageName");
            return (Criteria) this;
        }

        public Criteria andPackageNameNotBetween(String value1, String value2) {
            addCriterion("package_name not between", value1, value2, "packageName");
            return (Criteria) this;
        }

        public Criteria andMd5IsNull() {
            addCriterion("md5 is null");
            return (Criteria) this;
        }

        public Criteria andMd5IsNotNull() {
            addCriterion("md5 is not null");
            return (Criteria) this;
        }

        public Criteria andMd5EqualTo(String value) {
            addCriterion("md5 =", value, "md5");
            return (Criteria) this;
        }

        public Criteria andMd5NotEqualTo(String value) {
            addCriterion("md5 <>", value, "md5");
            return (Criteria) this;
        }

        public Criteria andMd5GreaterThan(String value) {
            addCriterion("md5 >", value, "md5");
            return (Criteria) this;
        }

        public Criteria andMd5GreaterThanOrEqualTo(String value) {
            addCriterion("md5 >=", value, "md5");
            return (Criteria) this;
        }

        public Criteria andMd5LessThan(String value) {
            addCriterion("md5 <", value, "md5");
            return (Criteria) this;
        }

        public Criteria andMd5LessThanOrEqualTo(String value) {
            addCriterion("md5 <=", value, "md5");
            return (Criteria) this;
        }

        public Criteria andMd5Like(String value) {
            addCriterion("md5 like", value, "md5");
            return (Criteria) this;
        }

        public Criteria andMd5NotLike(String value) {
            addCriterion("md5 not like", value, "md5");
            return (Criteria) this;
        }

        public Criteria andMd5In(List<String> values) {
            addCriterion("md5 in", values, "md5");
            return (Criteria) this;
        }

        public Criteria andMd5NotIn(List<String> values) {
            addCriterion("md5 not in", values, "md5");
            return (Criteria) this;
        }

        public Criteria andMd5Between(String value1, String value2) {
            addCriterion("md5 between", value1, value2, "md5");
            return (Criteria) this;
        }

        public Criteria andMd5NotBetween(String value1, String value2) {
            addCriterion("md5 not between", value1, value2, "md5");
            return (Criteria) this;
        }

        public Criteria andVersionNoIsNull() {
            addCriterion("version_no is null");
            return (Criteria) this;
        }

        public Criteria andVersionNoIsNotNull() {
            addCriterion("version_no is not null");
            return (Criteria) this;
        }

        public Criteria andVersionNoEqualTo(String value) {
            addCriterion("version_no =", value, "versionNo");
            return (Criteria) this;
        }

        public Criteria andVersionNoNotEqualTo(String value) {
            addCriterion("version_no <>", value, "versionNo");
            return (Criteria) this;
        }

        public Criteria andVersionNoGreaterThan(String value) {
            addCriterion("version_no >", value, "versionNo");
            return (Criteria) this;
        }

        public Criteria andVersionNoGreaterThanOrEqualTo(String value) {
            addCriterion("version_no >=", value, "versionNo");
            return (Criteria) this;
        }

        public Criteria andVersionNoLessThan(String value) {
            addCriterion("version_no <", value, "versionNo");
            return (Criteria) this;
        }

        public Criteria andVersionNoLessThanOrEqualTo(String value) {
            addCriterion("version_no <=", value, "versionNo");
            return (Criteria) this;
        }

        public Criteria andVersionNoLike(String value) {
            addCriterion("version_no like", value, "versionNo");
            return (Criteria) this;
        }

        public Criteria andVersionNoNotLike(String value) {
            addCriterion("version_no not like", value, "versionNo");
            return (Criteria) this;
        }

        public Criteria andVersionNoIn(List<String> values) {
            addCriterion("version_no in", values, "versionNo");
            return (Criteria) this;
        }

        public Criteria andVersionNoNotIn(List<String> values) {
            addCriterion("version_no not in", values, "versionNo");
            return (Criteria) this;
        }

        public Criteria andVersionNoBetween(String value1, String value2) {
            addCriterion("version_no between", value1, value2, "versionNo");
            return (Criteria) this;
        }

        public Criteria andVersionNoNotBetween(String value1, String value2) {
            addCriterion("version_no not between", value1, value2, "versionNo");
            return (Criteria) this;
        }

        public Criteria andLowestOsIsNull() {
            addCriterion("lowest_os is null");
            return (Criteria) this;
        }

        public Criteria andLowestOsIsNotNull() {
            addCriterion("lowest_os is not null");
            return (Criteria) this;
        }

        public Criteria andLowestOsEqualTo(Float value) {
            addCriterion("lowest_os =", value, "lowestOs");
            return (Criteria) this;
        }

        public Criteria andLowestOsNotEqualTo(Float value) {
            addCriterion("lowest_os <>", value, "lowestOs");
            return (Criteria) this;
        }

        public Criteria andLowestOsGreaterThan(Float value) {
            addCriterion("lowest_os >", value, "lowestOs");
            return (Criteria) this;
        }

        public Criteria andLowestOsGreaterThanOrEqualTo(Float value) {
            addCriterion("lowest_os >=", value, "lowestOs");
            return (Criteria) this;
        }

        public Criteria andLowestOsLessThan(Float value) {
            addCriterion("lowest_os <", value, "lowestOs");
            return (Criteria) this;
        }

        public Criteria andLowestOsLessThanOrEqualTo(Float value) {
            addCriterion("lowest_os <=", value, "lowestOs");
            return (Criteria) this;
        }

        public Criteria andLowestOsIn(List<Float> values) {
            addCriterion("lowest_os in", values, "lowestOs");
            return (Criteria) this;
        }

        public Criteria andLowestOsNotIn(List<Float> values) {
            addCriterion("lowest_os not in", values, "lowestOs");
            return (Criteria) this;
        }

        public Criteria andLowestOsBetween(Float value1, Float value2) {
            addCriterion("lowest_os between", value1, value2, "lowestOs");
            return (Criteria) this;
        }

        public Criteria andLowestOsNotBetween(Float value1, Float value2) {
            addCriterion("lowest_os not between", value1, value2, "lowestOs");
            return (Criteria) this;
        }

        public Criteria andHighestOsIsNull() {
            addCriterion("highest_os is null");
            return (Criteria) this;
        }

        public Criteria andHighestOsIsNotNull() {
            addCriterion("highest_os is not null");
            return (Criteria) this;
        }

        public Criteria andHighestOsEqualTo(Float value) {
            addCriterion("highest_os =", value, "highestOs");
            return (Criteria) this;
        }

        public Criteria andHighestOsNotEqualTo(Float value) {
            addCriterion("highest_os <>", value, "highestOs");
            return (Criteria) this;
        }

        public Criteria andHighestOsGreaterThan(Float value) {
            addCriterion("highest_os >", value, "highestOs");
            return (Criteria) this;
        }

        public Criteria andHighestOsGreaterThanOrEqualTo(Float value) {
            addCriterion("highest_os >=", value, "highestOs");
            return (Criteria) this;
        }

        public Criteria andHighestOsLessThan(Float value) {
            addCriterion("highest_os <", value, "highestOs");
            return (Criteria) this;
        }

        public Criteria andHighestOsLessThanOrEqualTo(Float value) {
            addCriterion("highest_os <=", value, "highestOs");
            return (Criteria) this;
        }

        public Criteria andHighestOsIn(List<Float> values) {
            addCriterion("highest_os in", values, "highestOs");
            return (Criteria) this;
        }

        public Criteria andHighestOsNotIn(List<Float> values) {
            addCriterion("highest_os not in", values, "highestOs");
            return (Criteria) this;
        }

        public Criteria andHighestOsBetween(Float value1, Float value2) {
            addCriterion("highest_os between", value1, value2, "highestOs");
            return (Criteria) this;
        }

        public Criteria andHighestOsNotBetween(Float value1, Float value2) {
            addCriterion("highest_os not between", value1, value2, "highestOs");
            return (Criteria) this;
        }

        public Criteria andSizeIsNull() {
            addCriterion("size is null");
            return (Criteria) this;
        }

        public Criteria andSizeIsNotNull() {
            addCriterion("size is not null");
            return (Criteria) this;
        }

        public Criteria andSizeEqualTo(Float value) {
            addCriterion("size =", value, "size");
            return (Criteria) this;
        }

        public Criteria andSizeNotEqualTo(Float value) {
            addCriterion("size <>", value, "size");
            return (Criteria) this;
        }

        public Criteria andSizeGreaterThan(Float value) {
            addCriterion("size >", value, "size");
            return (Criteria) this;
        }

        public Criteria andSizeGreaterThanOrEqualTo(Float value) {
            addCriterion("size >=", value, "size");
            return (Criteria) this;
        }

        public Criteria andSizeLessThan(Float value) {
            addCriterion("size <", value, "size");
            return (Criteria) this;
        }

        public Criteria andSizeLessThanOrEqualTo(Float value) {
            addCriterion("size <=", value, "size");
            return (Criteria) this;
        }

        public Criteria andSizeIn(List<Float> values) {
            addCriterion("size in", values, "size");
            return (Criteria) this;
        }

        public Criteria andSizeNotIn(List<Float> values) {
            addCriterion("size not in", values, "size");
            return (Criteria) this;
        }

        public Criteria andSizeBetween(Float value1, Float value2) {
            addCriterion("size between", value1, value2, "size");
            return (Criteria) this;
        }

        public Criteria andSizeNotBetween(Float value1, Float value2) {
            addCriterion("size not between", value1, value2, "size");
            return (Criteria) this;
        }

        public Criteria andIconPathIsNull() {
            addCriterion("icon_path is null");
            return (Criteria) this;
        }

        public Criteria andIconPathIsNotNull() {
            addCriterion("icon_path is not null");
            return (Criteria) this;
        }

        public Criteria andIconPathEqualTo(String value) {
            addCriterion("icon_path =", value, "iconPath");
            return (Criteria) this;
        }

        public Criteria andIconPathNotEqualTo(String value) {
            addCriterion("icon_path <>", value, "iconPath");
            return (Criteria) this;
        }

        public Criteria andIconPathGreaterThan(String value) {
            addCriterion("icon_path >", value, "iconPath");
            return (Criteria) this;
        }

        public Criteria andIconPathGreaterThanOrEqualTo(String value) {
            addCriterion("icon_path >=", value, "iconPath");
            return (Criteria) this;
        }

        public Criteria andIconPathLessThan(String value) {
            addCriterion("icon_path <", value, "iconPath");
            return (Criteria) this;
        }

        public Criteria andIconPathLessThanOrEqualTo(String value) {
            addCriterion("icon_path <=", value, "iconPath");
            return (Criteria) this;
        }

        public Criteria andIconPathLike(String value) {
            addCriterion("icon_path like", value, "iconPath");
            return (Criteria) this;
        }

        public Criteria andIconPathNotLike(String value) {
            addCriterion("icon_path not like", value, "iconPath");
            return (Criteria) this;
        }

        public Criteria andIconPathIn(List<String> values) {
            addCriterion("icon_path in", values, "iconPath");
            return (Criteria) this;
        }

        public Criteria andIconPathNotIn(List<String> values) {
            addCriterion("icon_path not in", values, "iconPath");
            return (Criteria) this;
        }

        public Criteria andIconPathBetween(String value1, String value2) {
            addCriterion("icon_path between", value1, value2, "iconPath");
            return (Criteria) this;
        }

        public Criteria andIconPathNotBetween(String value1, String value2) {
            addCriterion("icon_path not between", value1, value2, "iconPath");
            return (Criteria) this;
        }

        public Criteria andApkPathIsNull() {
            addCriterion("apk_path is null");
            return (Criteria) this;
        }

        public Criteria andApkPathIsNotNull() {
            addCriterion("apk_path is not null");
            return (Criteria) this;
        }

        public Criteria andApkPathEqualTo(String value) {
            addCriterion("apk_path =", value, "apkPath");
            return (Criteria) this;
        }

        public Criteria andApkPathNotEqualTo(String value) {
            addCriterion("apk_path <>", value, "apkPath");
            return (Criteria) this;
        }

        public Criteria andApkPathGreaterThan(String value) {
            addCriterion("apk_path >", value, "apkPath");
            return (Criteria) this;
        }

        public Criteria andApkPathGreaterThanOrEqualTo(String value) {
            addCriterion("apk_path >=", value, "apkPath");
            return (Criteria) this;
        }

        public Criteria andApkPathLessThan(String value) {
            addCriterion("apk_path <", value, "apkPath");
            return (Criteria) this;
        }

        public Criteria andApkPathLessThanOrEqualTo(String value) {
            addCriterion("apk_path <=", value, "apkPath");
            return (Criteria) this;
        }

        public Criteria andApkPathLike(String value) {
            addCriterion("apk_path like", value, "apkPath");
            return (Criteria) this;
        }

        public Criteria andApkPathNotLike(String value) {
            addCriterion("apk_path not like", value, "apkPath");
            return (Criteria) this;
        }

        public Criteria andApkPathIn(List<String> values) {
            addCriterion("apk_path in", values, "apkPath");
            return (Criteria) this;
        }

        public Criteria andApkPathNotIn(List<String> values) {
            addCriterion("apk_path not in", values, "apkPath");
            return (Criteria) this;
        }

        public Criteria andApkPathBetween(String value1, String value2) {
            addCriterion("apk_path between", value1, value2, "apkPath");
            return (Criteria) this;
        }

        public Criteria andApkPathNotBetween(String value1, String value2) {
            addCriterion("apk_path not between", value1, value2, "apkPath");
            return (Criteria) this;
        }

        public Criteria andImagePathIsNull() {
            addCriterion("image_path is null");
            return (Criteria) this;
        }

        public Criteria andImagePathIsNotNull() {
            addCriterion("image_path is not null");
            return (Criteria) this;
        }

        public Criteria andImagePathEqualTo(String value) {
            addCriterion("image_path =", value, "imagePath");
            return (Criteria) this;
        }

        public Criteria andImagePathNotEqualTo(String value) {
            addCriterion("image_path <>", value, "imagePath");
            return (Criteria) this;
        }

        public Criteria andImagePathGreaterThan(String value) {
            addCriterion("image_path >", value, "imagePath");
            return (Criteria) this;
        }

        public Criteria andImagePathGreaterThanOrEqualTo(String value) {
            addCriterion("image_path >=", value, "imagePath");
            return (Criteria) this;
        }

        public Criteria andImagePathLessThan(String value) {
            addCriterion("image_path <", value, "imagePath");
            return (Criteria) this;
        }

        public Criteria andImagePathLessThanOrEqualTo(String value) {
            addCriterion("image_path <=", value, "imagePath");
            return (Criteria) this;
        }

        public Criteria andImagePathLike(String value) {
            addCriterion("image_path like", value, "imagePath");
            return (Criteria) this;
        }

        public Criteria andImagePathNotLike(String value) {
            addCriterion("image_path not like", value, "imagePath");
            return (Criteria) this;
        }

        public Criteria andImagePathIn(List<String> values) {
            addCriterion("image_path in", values, "imagePath");
            return (Criteria) this;
        }

        public Criteria andImagePathNotIn(List<String> values) {
            addCriterion("image_path not in", values, "imagePath");
            return (Criteria) this;
        }

        public Criteria andImagePathBetween(String value1, String value2) {
            addCriterion("image_path between", value1, value2, "imagePath");
            return (Criteria) this;
        }

        public Criteria andImagePathNotBetween(String value1, String value2) {
            addCriterion("image_path not between", value1, value2, "imagePath");
            return (Criteria) this;
        }

        public Criteria andRankIsNull() {
            addCriterion("rank is null");
            return (Criteria) this;
        }

        public Criteria andRankIsNotNull() {
            addCriterion("rank is not null");
            return (Criteria) this;
        }

        public Criteria andRankEqualTo(Byte value) {
            addCriterion("rank =", value, "rank");
            return (Criteria) this;
        }

        public Criteria andRankNotEqualTo(Byte value) {
            addCriterion("rank <>", value, "rank");
            return (Criteria) this;
        }

        public Criteria andRankGreaterThan(Byte value) {
            addCriterion("rank >", value, "rank");
            return (Criteria) this;
        }

        public Criteria andRankGreaterThanOrEqualTo(Byte value) {
            addCriterion("rank >=", value, "rank");
            return (Criteria) this;
        }

        public Criteria andRankLessThan(Byte value) {
            addCriterion("rank <", value, "rank");
            return (Criteria) this;
        }

        public Criteria andRankLessThanOrEqualTo(Byte value) {
            addCriterion("rank <=", value, "rank");
            return (Criteria) this;
        }

        public Criteria andRankIn(List<Byte> values) {
            addCriterion("rank in", values, "rank");
            return (Criteria) this;
        }

        public Criteria andRankNotIn(List<Byte> values) {
            addCriterion("rank not in", values, "rank");
            return (Criteria) this;
        }

        public Criteria andRankBetween(Byte value1, Byte value2) {
            addCriterion("rank between", value1, value2, "rank");
            return (Criteria) this;
        }

        public Criteria andRankNotBetween(Byte value1, Byte value2) {
            addCriterion("rank not between", value1, value2, "rank");
            return (Criteria) this;
        }

        public Criteria andStatusIsNull() {
            addCriterion("status is null");
            return (Criteria) this;
        }

        public Criteria andStatusIsNotNull() {
            addCriterion("status is not null");
            return (Criteria) this;
        }

        public Criteria andStatusEqualTo(Integer value) {
            addCriterion("status =", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotEqualTo(Integer value) {
            addCriterion("status <>", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThan(Integer value) {
            addCriterion("status >", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("status >=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThan(Integer value) {
            addCriterion("status <", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThanOrEqualTo(Integer value) {
            addCriterion("status <=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusIn(List<Integer> values) {
            addCriterion("status in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotIn(List<Integer> values) {
            addCriterion("status not in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusBetween(Integer value1, Integer value2) {
            addCriterion("status between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotBetween(Integer value1, Integer value2) {
            addCriterion("status not between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andVersionDescIsNull() {
            addCriterion("version_desc is null");
            return (Criteria) this;
        }

        public Criteria andVersionDescIsNotNull() {
            addCriterion("version_desc is not null");
            return (Criteria) this;
        }

        public Criteria andVersionDescEqualTo(String value) {
            addCriterion("version_desc =", value, "versionDesc");
            return (Criteria) this;
        }

        public Criteria andVersionDescNotEqualTo(String value) {
            addCriterion("version_desc <>", value, "versionDesc");
            return (Criteria) this;
        }

        public Criteria andVersionDescGreaterThan(String value) {
            addCriterion("version_desc >", value, "versionDesc");
            return (Criteria) this;
        }

        public Criteria andVersionDescGreaterThanOrEqualTo(String value) {
            addCriterion("version_desc >=", value, "versionDesc");
            return (Criteria) this;
        }

        public Criteria andVersionDescLessThan(String value) {
            addCriterion("version_desc <", value, "versionDesc");
            return (Criteria) this;
        }

        public Criteria andVersionDescLessThanOrEqualTo(String value) {
            addCriterion("version_desc <=", value, "versionDesc");
            return (Criteria) this;
        }

        public Criteria andVersionDescLike(String value) {
            addCriterion("version_desc like", value, "versionDesc");
            return (Criteria) this;
        }

        public Criteria andVersionDescNotLike(String value) {
            addCriterion("version_desc not like", value, "versionDesc");
            return (Criteria) this;
        }

        public Criteria andVersionDescIn(List<String> values) {
            addCriterion("version_desc in", values, "versionDesc");
            return (Criteria) this;
        }

        public Criteria andVersionDescNotIn(List<String> values) {
            addCriterion("version_desc not in", values, "versionDesc");
            return (Criteria) this;
        }

        public Criteria andVersionDescBetween(String value1, String value2) {
            addCriterion("version_desc between", value1, value2, "versionDesc");
            return (Criteria) this;
        }

        public Criteria andVersionDescNotBetween(String value1, String value2) {
            addCriterion("version_desc not between", value1, value2, "versionDesc");
            return (Criteria) this;
        }

        public Criteria andManulScoreIsNull() {
            addCriterion("manul_score is null");
            return (Criteria) this;
        }

        public Criteria andManulScoreIsNotNull() {
            addCriterion("manul_score is not null");
            return (Criteria) this;
        }

        public Criteria andManulScoreEqualTo(Float value) {
            addCriterion("manul_score =", value, "manulScore");
            return (Criteria) this;
        }

        public Criteria andManulScoreNotEqualTo(Float value) {
            addCriterion("manul_score <>", value, "manulScore");
            return (Criteria) this;
        }

        public Criteria andManulScoreGreaterThan(Float value) {
            addCriterion("manul_score >", value, "manulScore");
            return (Criteria) this;
        }

        public Criteria andManulScoreGreaterThanOrEqualTo(Float value) {
            addCriterion("manul_score >=", value, "manulScore");
            return (Criteria) this;
        }

        public Criteria andManulScoreLessThan(Float value) {
            addCriterion("manul_score <", value, "manulScore");
            return (Criteria) this;
        }

        public Criteria andManulScoreLessThanOrEqualTo(Float value) {
            addCriterion("manul_score <=", value, "manulScore");
            return (Criteria) this;
        }

        public Criteria andManulScoreIn(List<Float> values) {
            addCriterion("manul_score in", values, "manulScore");
            return (Criteria) this;
        }

        public Criteria andManulScoreNotIn(List<Float> values) {
            addCriterion("manul_score not in", values, "manulScore");
            return (Criteria) this;
        }

        public Criteria andManulScoreBetween(Float value1, Float value2) {
            addCriterion("manul_score between", value1, value2, "manulScore");
            return (Criteria) this;
        }

        public Criteria andManulScoreNotBetween(Float value1, Float value2) {
            addCriterion("manul_score not between", value1, value2, "manulScore");
            return (Criteria) this;
        }

        public Criteria andScoreDateStartIsNull() {
            addCriterion("score_date_start is null");
            return (Criteria) this;
        }

        public Criteria andScoreDateStartIsNotNull() {
            addCriterion("score_date_start is not null");
            return (Criteria) this;
        }

        public Criteria andScoreDateStartEqualTo(Date value) {
            addCriterion("score_date_start =", value, "scoreDateStart");
            return (Criteria) this;
        }

        public Criteria andScoreDateStartNotEqualTo(Date value) {
            addCriterion("score_date_start <>", value, "scoreDateStart");
            return (Criteria) this;
        }

        public Criteria andScoreDateStartGreaterThan(Date value) {
            addCriterion("score_date_start >", value, "scoreDateStart");
            return (Criteria) this;
        }

        public Criteria andScoreDateStartGreaterThanOrEqualTo(Date value) {
            addCriterion("score_date_start >=", value, "scoreDateStart");
            return (Criteria) this;
        }

        public Criteria andScoreDateStartLessThan(Date value) {
            addCriterion("score_date_start <", value, "scoreDateStart");
            return (Criteria) this;
        }

        public Criteria andScoreDateStartLessThanOrEqualTo(Date value) {
            addCriterion("score_date_start <=", value, "scoreDateStart");
            return (Criteria) this;
        }

        public Criteria andScoreDateStartIn(List<Date> values) {
            addCriterion("score_date_start in", values, "scoreDateStart");
            return (Criteria) this;
        }

        public Criteria andScoreDateStartNotIn(List<Date> values) {
            addCriterion("score_date_start not in", values, "scoreDateStart");
            return (Criteria) this;
        }

        public Criteria andScoreDateStartBetween(Date value1, Date value2) {
            addCriterion("score_date_start between", value1, value2, "scoreDateStart");
            return (Criteria) this;
        }

        public Criteria andScoreDateStartNotBetween(Date value1, Date value2) {
            addCriterion("score_date_start not between", value1, value2, "scoreDateStart");
            return (Criteria) this;
        }

        public Criteria andScoreDateEndIsNull() {
            addCriterion("score_date_end is null");
            return (Criteria) this;
        }

        public Criteria andScoreDateEndIsNotNull() {
            addCriterion("score_date_end is not null");
            return (Criteria) this;
        }

        public Criteria andScoreDateEndEqualTo(Date value) {
            addCriterion("score_date_end =", value, "scoreDateEnd");
            return (Criteria) this;
        }

        public Criteria andScoreDateEndNotEqualTo(Date value) {
            addCriterion("score_date_end <>", value, "scoreDateEnd");
            return (Criteria) this;
        }

        public Criteria andScoreDateEndGreaterThan(Date value) {
            addCriterion("score_date_end >", value, "scoreDateEnd");
            return (Criteria) this;
        }

        public Criteria andScoreDateEndGreaterThanOrEqualTo(Date value) {
            addCriterion("score_date_end >=", value, "scoreDateEnd");
            return (Criteria) this;
        }

        public Criteria andScoreDateEndLessThan(Date value) {
            addCriterion("score_date_end <", value, "scoreDateEnd");
            return (Criteria) this;
        }

        public Criteria andScoreDateEndLessThanOrEqualTo(Date value) {
            addCriterion("score_date_end <=", value, "scoreDateEnd");
            return (Criteria) this;
        }

        public Criteria andScoreDateEndIn(List<Date> values) {
            addCriterion("score_date_end in", values, "scoreDateEnd");
            return (Criteria) this;
        }

        public Criteria andScoreDateEndNotIn(List<Date> values) {
            addCriterion("score_date_end not in", values, "scoreDateEnd");
            return (Criteria) this;
        }

        public Criteria andScoreDateEndBetween(Date value1, Date value2) {
            addCriterion("score_date_end between", value1, value2, "scoreDateEnd");
            return (Criteria) this;
        }

        public Criteria andScoreDateEndNotBetween(Date value1, Date value2) {
            addCriterion("score_date_end not between", value1, value2, "scoreDateEnd");
            return (Criteria) this;
        }

        public Criteria andScoreAvgIsNull() {
            addCriterion("score_avg is null");
            return (Criteria) this;
        }

        public Criteria andScoreAvgIsNotNull() {
            addCriterion("score_avg is not null");
            return (Criteria) this;
        }

        public Criteria andScoreAvgEqualTo(Float value) {
            addCriterion("score_avg =", value, "scoreAvg");
            return (Criteria) this;
        }

        public Criteria andScoreAvgNotEqualTo(Float value) {
            addCriterion("score_avg <>", value, "scoreAvg");
            return (Criteria) this;
        }

        public Criteria andScoreAvgGreaterThan(Float value) {
            addCriterion("score_avg >", value, "scoreAvg");
            return (Criteria) this;
        }

        public Criteria andScoreAvgGreaterThanOrEqualTo(Float value) {
            addCriterion("score_avg >=", value, "scoreAvg");
            return (Criteria) this;
        }

        public Criteria andScoreAvgLessThan(Float value) {
            addCriterion("score_avg <", value, "scoreAvg");
            return (Criteria) this;
        }

        public Criteria andScoreAvgLessThanOrEqualTo(Float value) {
            addCriterion("score_avg <=", value, "scoreAvg");
            return (Criteria) this;
        }

        public Criteria andScoreAvgIn(List<Float> values) {
            addCriterion("score_avg in", values, "scoreAvg");
            return (Criteria) this;
        }

        public Criteria andScoreAvgNotIn(List<Float> values) {
            addCriterion("score_avg not in", values, "scoreAvg");
            return (Criteria) this;
        }

        public Criteria andScoreAvgBetween(Float value1, Float value2) {
            addCriterion("score_avg between", value1, value2, "scoreAvg");
            return (Criteria) this;
        }

        public Criteria andScoreAvgNotBetween(Float value1, Float value2) {
            addCriterion("score_avg not between", value1, value2, "scoreAvg");
            return (Criteria) this;
        }

        public Criteria andDownloadSumIsNull() {
            addCriterion("download_sum is null");
            return (Criteria) this;
        }

        public Criteria andDownloadSumIsNotNull() {
            addCriterion("download_sum is not null");
            return (Criteria) this;
        }

        public Criteria andDownloadSumEqualTo(Long value) {
            addCriterion("download_sum =", value, "downloadSum");
            return (Criteria) this;
        }

        public Criteria andDownloadSumNotEqualTo(Long value) {
            addCriterion("download_sum <>", value, "downloadSum");
            return (Criteria) this;
        }

        public Criteria andDownloadSumGreaterThan(Long value) {
            addCriterion("download_sum >", value, "downloadSum");
            return (Criteria) this;
        }

        public Criteria andDownloadSumGreaterThanOrEqualTo(Long value) {
            addCriterion("download_sum >=", value, "downloadSum");
            return (Criteria) this;
        }

        public Criteria andDownloadSumLessThan(Long value) {
            addCriterion("download_sum <", value, "downloadSum");
            return (Criteria) this;
        }

        public Criteria andDownloadSumLessThanOrEqualTo(Long value) {
            addCriterion("download_sum <=", value, "downloadSum");
            return (Criteria) this;
        }

        public Criteria andDownloadSumIn(List<Long> values) {
            addCriterion("download_sum in", values, "downloadSum");
            return (Criteria) this;
        }

        public Criteria andDownloadSumNotIn(List<Long> values) {
            addCriterion("download_sum not in", values, "downloadSum");
            return (Criteria) this;
        }

        public Criteria andDownloadSumBetween(Long value1, Long value2) {
            addCriterion("download_sum between", value1, value2, "downloadSum");
            return (Criteria) this;
        }

        public Criteria andDownloadSumNotBetween(Long value1, Long value2) {
            addCriterion("download_sum not between", value1, value2, "downloadSum");
            return (Criteria) this;
        }

        public Criteria andNewVersionIsNull() {
            addCriterion("new_version is null");
            return (Criteria) this;
        }

        public Criteria andNewVersionIsNotNull() {
            addCriterion("new_version is not null");
            return (Criteria) this;
        }

        public Criteria andNewVersionEqualTo(Integer value) {
            addCriterion("new_version =", value, "newVersion");
            return (Criteria) this;
        }

        public Criteria andNewVersionNotEqualTo(Integer value) {
            addCriterion("new_version <>", value, "newVersion");
            return (Criteria) this;
        }

        public Criteria andNewVersionGreaterThan(Integer value) {
            addCriterion("new_version >", value, "newVersion");
            return (Criteria) this;
        }

        public Criteria andNewVersionGreaterThanOrEqualTo(Integer value) {
            addCriterion("new_version >=", value, "newVersion");
            return (Criteria) this;
        }

        public Criteria andNewVersionLessThan(Integer value) {
            addCriterion("new_version <", value, "newVersion");
            return (Criteria) this;
        }

        public Criteria andNewVersionLessThanOrEqualTo(Integer value) {
            addCriterion("new_version <=", value, "newVersion");
            return (Criteria) this;
        }

        public Criteria andNewVersionIn(List<Integer> values) {
            addCriterion("new_version in", values, "newVersion");
            return (Criteria) this;
        }

        public Criteria andNewVersionNotIn(List<Integer> values) {
            addCriterion("new_version not in", values, "newVersion");
            return (Criteria) this;
        }

        public Criteria andNewVersionBetween(Integer value1, Integer value2) {
            addCriterion("new_version between", value1, value2, "newVersion");
            return (Criteria) this;
        }

        public Criteria andNewVersionNotBetween(Integer value1, Integer value2) {
            addCriterion("new_version not between", value1, value2, "newVersion");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNull() {
            addCriterion("create_time is null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNotNull() {
            addCriterion("create_time is not null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeEqualTo(Date value) {
            addCriterion("create_time =", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotEqualTo(Date value) {
            addCriterion("create_time <>", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThan(Date value) {
            addCriterion("create_time >", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("create_time >=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThan(Date value) {
            addCriterion("create_time <", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("create_time <=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIn(List<Date> values) {
            addCriterion("create_time in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotIn(List<Date> values) {
            addCriterion("create_time not in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeBetween(Date value1, Date value2) {
            addCriterion("create_time between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("create_time not between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIsNull() {
            addCriterion("update_time is null");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIsNotNull() {
            addCriterion("update_time is not null");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeEqualTo(Date value) {
            addCriterion("update_time =", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotEqualTo(Date value) {
            addCriterion("update_time <>", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeGreaterThan(Date value) {
            addCriterion("update_time >", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("update_time >=", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLessThan(Date value) {
            addCriterion("update_time <", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLessThanOrEqualTo(Date value) {
            addCriterion("update_time <=", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIn(List<Date> values) {
            addCriterion("update_time in", values, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotIn(List<Date> values) {
            addCriterion("update_time not in", values, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeBetween(Date value1, Date value2) {
            addCriterion("update_time between", value1, value2, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotBetween(Date value1, Date value2) {
            addCriterion("update_time not between", value1, value2, "updateTime");
            return (Criteria) this;
        }

        public Criteria andCreateUserIsNull() {
            addCriterion("create_user is null");
            return (Criteria) this;
        }

        public Criteria andCreateUserIsNotNull() {
            addCriterion("create_user is not null");
            return (Criteria) this;
        }

        public Criteria andCreateUserEqualTo(String value) {
            addCriterion("create_user =", value, "createUser");
            return (Criteria) this;
        }

        public Criteria andCreateUserNotEqualTo(String value) {
            addCriterion("create_user <>", value, "createUser");
            return (Criteria) this;
        }

        public Criteria andCreateUserGreaterThan(String value) {
            addCriterion("create_user >", value, "createUser");
            return (Criteria) this;
        }

        public Criteria andCreateUserGreaterThanOrEqualTo(String value) {
            addCriterion("create_user >=", value, "createUser");
            return (Criteria) this;
        }

        public Criteria andCreateUserLessThan(String value) {
            addCriterion("create_user <", value, "createUser");
            return (Criteria) this;
        }

        public Criteria andCreateUserLessThanOrEqualTo(String value) {
            addCriterion("create_user <=", value, "createUser");
            return (Criteria) this;
        }

        public Criteria andCreateUserLike(String value) {
            addCriterion("create_user like", value, "createUser");
            return (Criteria) this;
        }

        public Criteria andCreateUserNotLike(String value) {
            addCriterion("create_user not like", value, "createUser");
            return (Criteria) this;
        }

        public Criteria andCreateUserIn(List<String> values) {
            addCriterion("create_user in", values, "createUser");
            return (Criteria) this;
        }

        public Criteria andCreateUserNotIn(List<String> values) {
            addCriterion("create_user not in", values, "createUser");
            return (Criteria) this;
        }

        public Criteria andCreateUserBetween(String value1, String value2) {
            addCriterion("create_user between", value1, value2, "createUser");
            return (Criteria) this;
        }

        public Criteria andCreateUserNotBetween(String value1, String value2) {
            addCriterion("create_user not between", value1, value2, "createUser");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}