package com.mapbar.grant.test;

import java.io.UnsupportedEncodingException;


import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/test")
public class TestController {
	@RequestMapping(value="/login")
	public String login(TestUser testUser,Model model) throws UnsupportedEncodingException{
		System.out.println(testUser);
		model.addAttribute("msg", "model add 一个消息");
		model.addAttribute("testUser", testUser);
		return "test/login";
	}
}
