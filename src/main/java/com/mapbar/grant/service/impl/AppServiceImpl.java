package com.mapbar.grant.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mapbar.grant.dao.AppMapper;
import com.mapbar.grant.service.AppService;

@Service
public class AppServiceImpl implements AppService{

	@Autowired
	AppMapper appMapper;

	public String test() {
		return appMapper.test();
	}
	
	
}
