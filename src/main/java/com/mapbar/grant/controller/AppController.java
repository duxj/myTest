package com.mapbar.grant.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mapbar.grant.service.AppService;

 
@Controller
@RequestMapping("app")
public class AppController {
	@Autowired
	private AppService appService;
	@Autowired
	private RedisTemplate redisTemplate;
	
	@ResponseBody
    @RequestMapping(value="index")
    public String showIndex(){
		String name = appService.test();
		
		redisTemplate.opsForHash().put("tt", "age", 25);
		System.out.println("11111111111");
		System.out.println(redisTemplate.opsForHash().get("tt", "age"));
		System.out.println(redisTemplate.opsForHash().get("tt", "name"));
		
    	return name;
    }
  
  	
}